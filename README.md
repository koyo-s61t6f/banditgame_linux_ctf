># OverTheWire WarGame解いてみた

## 【Discription】

<p>これはOverTheWireにあるWarGameをどのように解いたのかまとめたものである。<br>※LinuxのCTFの様なもの</p>


【サイトURL：】 https://overthewire.org/wargames/

>## 問題数

||||||||||||
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|[Lv-0](#lv-0)|[Lv-1](#lv-1)|[Lv-2](#lv-2)|[Lv-3](#lv-3)|[Lv-4](#lv-4)|[Lv-5](#lv-5)|[Lv-6](#lv-6)|[Lv-7](#lv-7)|[Lv-8](#lv-8)|[Lv-9](#lv-9)|[Lv-10](#lv-10)|
|[Lv-11](#lv-11) |[Lv-12](#lv-12) |[Lv-13](#lv-13)|[Lv-14](#lv-14) |[Lv-15](#lv-15)|[Lv-16](#lv-16)|[Lv-17](#lv-17) |[Lv-18](#lv-18)|[Lv-19](#lv-19)|[Lv-20](#lv-20)|[Lv-21](#lv-21)|
|[Lv-22](#lv-22)|[Lv-23](#lv-23)|[Lv-24](#lv-24)|[Lv-25](#lv-25)|[Lv-26](#lv-26)|[Lv-27](#lv-27)|[Lv-28](#lv-28)|[Lv-29](#lv-29) |[Lv-30](#lv-30) |[Lv-31](#lv-31)|[Lv-32](#lv-32)|


## 【躓いたとき参考にしたサイト】
- https://github.com/Whimmery/CTF-Bandit
- https://kongwenbin.wordpress.com/2016/09/10/overthewire-bandit-level-23-to-level-24/

<br>

># Lv-0

### 【問題】

<p style="font-size:90%">The password for the next level is stored in a file called readme located in the home directory. Use this password to log into bandit1 using SSH. Whenever you find a password for a level, use SSH (on port 2220) to log into that level and continue the game.</p>
<br>


### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、ホームディレクトリにあるreadmeというファイルに保存されています 。このパスワードを使用して、SSHを使用してbandit1にログインします。レベルのパスワードを見つけたら、SSH（ポート2220）を使用してそのレベルにログインし、ゲームを続行します。a</p>
<br>

### 【回答】
<span>これはとても簡単です。<br>指定されてるホスト、ポートへ与えられたユーザ、パスワードでSSH接続しホームにあるファイルを確認するだけ</span>

<br>

```
※ssh ユーザ@ホスト名 -p ポート番号
# ssh bandit0@bandit.labs.overthewire -p 2220

※cat （オプション）ファイル名
[bandit0@bandit:~$] cat readme
```

># Lv-1

### 【問題】
<p style="font-size:90%;">The password for the next level is stored in a file called - located in the home directory</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、ホームディレクトリにある--というファイルに保存されます。</p>

### 【回答】

<span>これはファイル名に"-"などの禁止文字が入っているファイルを開く必要があります。<br>
普通にcatやfileコマンドをつかうとエラーが出てしまいます。<br>なのでここで./"やリダイレクトを使います</span>

```
※file （オプション）ファイル名
[bandit1@bandit:~$] cat ./readme
[bandit1@bandit:~$] cat < readme
[bandit1@bandit:~$] file ./readme
```
># Lv-2
### 【問題】

<p style="font-size:90%;">The password for the next level is stored in a file called spaces in this filename located in the home directory</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、ホームディレクトリにあるこのファイル名のスペースと呼ばれるファイルに保存されます</p>
<br>

### 【回答】

<span>これは問題分をすべて翻訳してしまうとわかりづらいので**spaces in this filename**は訳さないでください<br>※これはファイル名です<br>
今回はファイル名にスペースなどが入ったものをどう開くかという問題です。<br>Lv-1同様そのままcatやfileコマンドでは開けません。なのでファイル名を参照する時にスペースがある左に<strong>”/”</strong> を入れてみましょう
</span>

<br>

```
[bandit2@bandit:~$] cat spaces\ in\ this\ filename
```
># Lv-3

### 【問題】
<p style="font-size:90%;">The password for the next level is stored in a hidden file in the inhere directory.</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、 inhereディレクトリの隠しファイルに保存されます 。</p>

### 【回答】

<span>これはパスワードが記されているファイルが通常のファイルではなく隠しファイルとして保存されているので、普通にlsだけだと表示はされません。<br>なのでlsにオプションをつけてあげて隠しファイルも表示してくれるようにしましょう<br>ファイルが見つけれたらあとは簡単ですcatでパスワードを出力してください</span>

```
[bandit3@bandit:~/inhere$] ls -a
[bandit3@bandit:~/inhere$] cat 隠しファイル名
```


># Lv-4
### 【問題】

<p style="font-size:90%;">The password for the next level is stored in the only human-readable file in the inhere directory.<br>Tip: if your terminal is messed up, try the “reset” command.</p>
<br>

### 【翻訳】

<p style="font-size:120%;">次のレベルのパスワードは、 inhereディレクトリにある人間が読める形式のファイルにのみ保存されます。ヒント：端末が混乱している場合は、「リセット」コマンドを試してください。</p>
<br>

### 回答

<span>今回の問題はinhereディレクトリ内に複数のファイルが存在していますが、その中に一つだけ人間が読める形式のファイルがありそれを見つける問題です<br>しかし1つ1つ中身をみてもいいのですが手間なのでfileコマンドを使いそれぞれのファイルの形式を見てあたりをつけましょう<br>見つけることができたらあとは簡単です、一応ファイル名が禁止文字を使っているのでLv-同様の方法で表示しましょう</span>

<br>

```
[bandit4@bandit:~/inhere$] file ./-file*
[bandit4@bandit:~/inhere$]cat ./file??
```

># Lv-5

### 【問題】

<p style="font-size:90%;">The password for the next level is stored in a file somewhere under the inhere directory and has all of the following properties:

- human-readable
- 1033 bytes in size
- not executable</p>

<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、 inhereディレクトリの下のファイルに保存され、次のすべてのプロパティがあります。

- 人間が読める形式
- サイズは1033バイト
- 実行不可</p>

### 回答
<span>指定されたファイルを数あるディレクトリ内から探せという問題です。lsして各ディレクトリを見てみるとわかりますが、割と多いのでこれを一つ一つ見ていくのは困難です。なので指定部分にある**サイズ1033**を使ってファイルを探索してみましょう</span>
```
※find オプション ファイル名
※find -size 数値c(ちょうどなら)
[bandit5@bandit:~/inhere$]find -size 1033c
```

># 問題 Lv-6
### 【問題】
<p style="font-size:90%;">The password for the next level is stored somewhere on the server and has all of the following properties

- owned by user bandit7
- owned by group bandit6
- 33 bytes in size</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードはサーバーのどこかに保存され、次のすべてのプロパティがあります。

- ユーザーbandit7が所有
- グループbandit6が所有
- サイズは33バイト</p>

### 回答
<span>今度のはLv-の延長線ですね。指定にユーザやグループなどが追加されてます<br>
それでは同じようにfindコマンドで指定のパラメータを入れてみましょう<br>どうでしょうか、ヒットはしますが、ファイルがありすぎますね<br>ほとんどが**Permission denied**のエラーが表示されているので**これがないファイル**としてさらに絞ってみましょう<br>そうするとヒットするのでパスワードを取得しましょう</span>

```
※find -size 数値c -user ユーザ -group グループ
[bandit5@bandit:~/inhere$]find -size 33c -user bandit7 -group bandit6

※2>/dev/null➡エラー出力を消す
[bandit5@bandit:~/inhere$]find -size 33c -user bandit7 -group bandit6 2>/dev/null
```

># Lv-7
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in the file data.txt next to the word millionth</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、 millionth という単語の横にあるdata.txtファイルに保存されます。</p>

### 回答
<span>この問題はdata.txtの中から指定の文字列を見つける問題です<br>やり方は2通りありfind、grepコマンドを使うパターンとviエディタで検索をかけるパターンです<br>探していたパスワードを見つけれましたね</span>

```
find,grepコマンドの場合
※grep 文字列
[bandit7@bandit:~$]cat data.txt | grep millionth

viエディタを使った場合
[bandit7@bandit:~/$]vi data.txt
ESCキー 
:/millionth
```

># Lv-8
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in the file data.txt and is the only line of text that occurs only once</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードはファイルdata.txt に保存され、1回だけ出現する唯一のテキスト行です。</p>

### 回答
<span>今回の問題は少しやっかいです<br>テキスト内に1つしかない文字列を探さないといけません。こういう時はuniqコマンドを使います<br>uniqで絞る前にsortで整理してからの方が正しい情報になります。<br>さらにuniq -uオプションで重複していないものだけを表示させます。これでパスワードがわかりましたね</span>
```
※uniq オプション ファイル名
※sort ファイル名
[bandit8@bandit:~$]sort data.txt |uniq -u
```

># Lv-9

### 【問題】
<p style="font-size:90%;">The password for the next level is stored in the file data.txt in one of the few human-readable strings, preceded by several ‘=’ characters.</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、いくつかの「=」文字が前に付いた、人間が読める数少ない文字列の1つでファイルdata.txtに保存されます。</p>

### 回答
<span>今回の問題はさらに厄介です。なぜならデータがバイナリなのでgrepコマンドのみで探すことができません。<br>バイナリファイルを表示したい場合は**strings**コマンドを使います。<br>表示させた後にgrepコマンドを使ってパスワードを取得します</span>

```
[bandit9@bandit:~$]grep = data.txt
[bandit8@bandit:~$]Binary file data.txt matches

※strings オプション ファイル名
[bandit8@bandit:~$]strings data.txt | grep =
```

># Lv-10
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in the file data.txt, which contains base64 encoded data</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、 base64でエンコードされたデータを含むファイルdata.txtに保存されます</p>
<br>

### 【回答】
<span>これはbase64でエンコード(暗号化)された文字列をデコード（復号)してパスワードを取得する問題です。base64でエンコード、デコードは**base64**コマンドをつかいます</span>

<br>

```
[bandit10@bandit:~$]cat data.txt
[bandit20@bandit:~$]VGhlIHBhc3N3b3JkIGlzIElGdWt3S0dzRlc4TU9xM0lSRnFyeEUxaHhUTkViVVBSCg==

※base64 オプション ファイル名
[bandit8@bandit:~$]base64 -d data.txt
```

># Lv-11
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in the file data.txt, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、ファイルdata.txtに保存されます。ここでは、すべての小文字（az）と大文字（AZ）の文字が13桁回転しています。</p>
<br>

### 【回答】

<span>今度の問題はdata.txtファイルの文字列が13文字文ずれている（シーザー暗号）のでこれを戻してパスワードを取得する問題
である<br>Linuxにはrotという今回の問題に適した便利なコマンドがあるがホストにはこれはインストールされていないのでtrコマンドで同じことを実装します<br>13個分とはNからZ、AからMです</span>

<br>

```
bandit11@bandit:~$ cat data.txt | rot13
> -bash: rot13: command not found
bandit11@bandit:~$ cat data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'
```

># Lv-12
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in the file data.txt, which is a hexdump of a file that has been repeatedly compressed. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!)</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、繰り返し圧縮されたファイルの16進ダンプであるファイルdata.txtに保存されます。このレベルでは、mkdirを使用して作業できるディレクトリを/tmpの下に作成すると便利な場合があります。例：mkdir / tmp/myname123。次に、cpを使用してデータファイルをコピーし、mvを使用して名前を変更します（マンページを読んでください）。</p>
<br>

### 【回答】
<span>今回の問題はとても難しく、ファイルは16進数のファイルとして保存されているのでバイナリに変換しましょう。<br>バイナリに変換するときはxddコマンドを使います。<br>変換したファイルを見てみると何が書いてあるかさっぱりなので一度ファイルに起こしてfileコマンドを使いファイル形式を見てみましょう。<br>※ファイル操作をするので操作できる環境に移動しましょう。/tmp/にディレクトリを作成しdata.txtをcpしてそこで作業してください。<br>すると形式がgzipになっているのが確認できるのでgzipコマンドで展開しましょう。<br>同じように展開したファイル形式を見ると今度はbzipになってます</span>あとは同じように繰り返し大本までひたすらに展開

<br>

```
bandit12@bandit:$ mkdir /tmp/bandit12; cd /tmp/bandit12 
bandit12@bandit:/tmp/bandit12$ cp ~/data.txt /tmp/bandit12
bandit12@bandit:/tmp/bandit12$ cat data.txt

bandit12@bandit:/tmp/bandit12$ xxd -r data.txt
bandit12@bandit:/tmp/bandit12$ cat data.txt > data_rev
bandit12@bandit:/tmp/bandit12$ file data_rev

bandit12@bandit:/tmp/bandit12$ cat data_rev > data_gz.gz
bandit12@bandit:/tmp/bandit12$ gzip -d data_gz.gz
bandit12@bandit:/tmp/bandit12$ file data_gz

bandit12@bandit:/tmp/bandit12$ cat data_gz > data_bz.bz2
bandit12@bandit:/tmp/bandit12$ bizp2 -d data_bz.bz2
bandit12@bandit:/tmp/bandit12$ file data_bz

bandit12@bandit:/tmp/bandit12$ cat data_bz > data_gz2.gz
bandit12@bandit:/tmp/bandit12$ gzip -d data_gz2.gz
bandit12@bandit:/tmp/bandit12$ file data_gz2.gz

bandit12@bandit:/tmp/bandit12$ cat data_gz2 > data_tar.tar
bandit12@bandit:/tmp/bandit12$ tar -vfx data_tar.tar
bandit12@bandit:/tmp/bandit12$ file data5.bin
bandit12@bandit:/tmp/bandit12$ tar -vfx data5.bin
bandit12@bandit:/tmp/bandit12$ file data6.bin

bandit12@bandit:/tmp/bandit12$ cat data6.bin > data_bz2.bz
bandit12@bandit:/tmp/bandit12$ bzip2 -d data_bz2.bz
bandit12@bandit:/tmp/bandit12$ file data_bz2

bandit12@bandit:/tmp/bandit12$ cat data_bz2 > data_tar2.data_tar
bandit12@bandit:/tmp/bandit12$ tar -vfx data_tar2.tar
bandit12@bandit:/tmp/bandit12$ file data8.bin

bandit12@bandit:/tmp/bandit12$ cat data8.bin > data_gz3.gz
bandit12@bandit:/tmp/bandit12$ gzip -d data_gz3.gz
bandit12@bandit:/tmp/bandit12$ file data_gz3

bandit12@bandit:/tmp/bandit12$ cat data_gz3
```

># Lv-13
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in /etc/bandit_pass/bandit14 and can only be read by user bandit14. For this level, you don’t get the next password, but you get a private SSH key that can be used to log into the next level. Note: localhost is a hostname that refers to the machine you are working on</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは /etc/ bandit_pass / bandit14に保存され、ユーザーbandit14のみが読み取ることができます。このレベルでは、次のパスワードは取得できませんが、次のレベルへのログインに使用できるSSH秘密キーは取得できます。 注： localhostは、作業しているマシンを参照するホスト名です。</p>
<br>

### 【回答】
<span>今回の問題はホームディレクトリにssh秘密鍵が入っているのでこれをホスト側に登録して接続するだけですね。<br>ホスト側の操作になるのでbandit13にあるsshファイルの内容はコピーしておきましょう<br>/.ssh/内にid_rsaがない人はid_rsaファイルを作成しキーの内容をコピーするだけで良いです。<br>※id_rsaが既にありキーファイルに名前をつけている人は~/.ssh/configファイルにその内容を記述しましょう<br>ログインできたらbandit14のパスワードは/etc/bandit_pass/bandit14の中にあるので保管しておきましょう</span>

<br>

```
User@PC:$ cd ~/.ssh
User@PC:~/.ssh$ vi id_rsa_bandit14
User@PC:$ vi config
User@PC:$ chmod 600 id_rsa_bandit14
User@PC:$ ssh bandit14@bandit.labs.overthewire -p 2220
```

### ~/.ssh/config
```
Host bandit.labs.overthewire
  Hostname bandit.labs.overthewire
  IdentityFile ~/.ssh/id_rsa_bandit14
  User bandit14
```

># Lv-14
### 【問題】
<p style="font-size:90%;">The password for the next level can be retrieved by submitting the password of the current level to port 30000 on localhost.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、現在のレベルのパスワードをローカルホストのポート30000に送信することで取得できます。</p>
<br>

### 【回答】
<span>これは指定されたポートへ通信し、bandit14のパスワードを送ってあげるとbandit15のパスワードがレスポンスとして帰ってくる問題です<br>ポート通信はncコマンドを使います</span>

<br>

```
※nc ホスト名 ポート番号
bandit14@bandit:~$ cat /etc/bandit_pass/bandit14 | nc localhost 30000
```

># Lv-15
### 【問題】
<p style="font-size:90%;">The password for the next level can be retrieved by submitting the password of the current level to port 30001 on localhost using SSL encryption.<br>
Helpful note: Getting “HEARTBEATING” and “Read R BLOCK”? Use -ign_eof and read the “CONNECTED COMMANDS” section in the manpage. Next to ‘R’ and ‘Q’, the ‘B’ command also works in this version of that command…</p>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、 SSL暗号化を使用してローカルホストのポート30001に現在のレベルのパスワードを送信することで取得できます。</p>


### 【回答】
<span>問題文の通りssl通信をしてパスワードを送りましょう。<br>ssl通信のためのコマンドはopensslです,さらに今回は接続する側なのでs_clientを使います</span>

```
※openssl s_client オプション ホスト:ポート
bandit15@bandit:~$openssl s_client -connect localhost:30001
>　bandit15のパスワード
```

># Lv-16
### 【問題】
<p style="font-size:90%;">The credentials for the next level can be retrieved by submitting the password of the current level to a port on localhost in the range 31000 to 32000. First find out which of these ports have a server listening on them. Then find out which of those speak SSL and which don’t. There is only 1 server that will give the next credentials, the others will simply send back to you whatever you send to it.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルの資格情報は、現在のレベルのパスワードをローカルホストの31000〜32000の範囲のポートに送信することで取得できます。まず、これらのポートのどれにサーバーがリッスンしているのかを調べます。次に、SSLを話すものと話さないものを見つけます。次のクレデンシャルを提供するサーバーは1つだけで、他のサーバーは、送信したものをすべて返送します。</p>
<br>

### 【回答】
<span>今回の問題は前回の応用です。開いているポートを探しその中でssl通信できるものを探します。<br>通信できたものにbandit16のパスワードを送ることでパスワードを取得することができます<br>開いているポートを調べるためにはnmapコマンドを使います
</span>


<br>

```
※nmap オプション ポート範囲(-) ホスト名
bandit16@bandit:~$ nmap -p 31000-32000 localhost
bandit16@bandit:~$ openssl s_client -connect localhost:開いているポート
> bandit16のパスワード
```
># Lv-17
### 【問題】
<p style="font-size:90%;">There are 2 files in the homedirectory: passwords.old and passwords.new. The password for the next level is in passwords.new and is the only line that has been changed between passwords.old and passwords.new<br>
NOTE: if you have solved this level and see ‘Byebye!’ when trying to log into bandit18, this is related to the next level, bandit19</p>
<br>

### 【翻訳】
<p style="font-size:120%;">ホームディレクトリには、 passwords.oldとpasswords.newの2つのファイルがあります。次のレベルのパスワードは passwords.newにあり、 passwords.oldとpasswords.newの間で変更された唯一の行 です。

注：このレベルを解決し、「Byebye！」を参照した場合 bandit18にログインしようとすると、これは次のレベルであるbandit19に関連します。</p>
<br>

### 【回答】
<span>前回パスワードの代わりにsshの秘密鍵を手に入れたので登録してssh通信しましょう。<br>この問題はホームディレクトリにある二つのファイルの差分を見つけそれがパスワードであるという問題です。<br>ファイルの差分を調べるためにはdiffコマンドを使います</span>

<br>

```
※diff ファイル1 ファイル2
bandit17@bandit:~$ diff passwords.new passwords.old
```

># Lv-18
### 【問題】
<p style="font-size:90%;">The password for the next level is stored in a file readme in the homedirectory. Unfortunately, someone has modified .bashrc to log you out when you log in with SSH.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルのパスワードは、ホームディレクトリのファイルreadmeに保存されます。残念ながら、 SSHでログインしたときにログアウトするように.bashrcが変更されました。</p>
<br>

### 【回答】
<span>今回の問題はbandit19にログインするとそのままログアウトされてしまうのでログインしつつコマンドを操作し、ホームディレクトリにあるreadmeファイルの中身を確認する問題です。<br>ssh接続しながらコマンド操作するためには通信先の後に操作したいコマンドを追記するだけです</span>

<br>

```
User@PC:~/$ ssh bandit18@bandit.labs.overthewire.org -p 2220 cat readme
```

># Lv-19
### 【問題】
<p style="font-size:90%;">To gain access to the next level, you should use the setuid binary in the homedirectory. Execute it without arguments to find out how to use it. The password for this level can be found in the usual place (/etc/bandit_pass), after you have used the setuid binary.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">次のレベルにアクセスするには、ホームディレクトリのsetuidバイナリを使用する必要があります。引数なしで実行して、使用方法を確認してください。このレベルのパスワードは、setuidバイナリを使用した後、通常の場所（/ etc / bandit_pass）にあります</p>
<br>

### 【回答】
<span>今回の問題はsetuidファイルを使ってパスワードを取得する問題です。<br>setuidとは実行する際、決められた権限で実行できるようにしたもの。※sudoみたいなもの？<br>なのでこれを使ってbandit20のパスワードを取得しましょう</span>

<br>

```
bandit19@bandit:~$ ./bandit20-do cat /etc/bandit_pass/bandit20
```

># Lv-20
### 【問題】
<p style="font-size:90%;">There is a setuid binary in the homedirectory that does the following: it makes a connection to localhost on the port you specify as a commandline argument. It then reads a line of text from the connection and compares it to the password in the previous level (bandit20). If the password is correct, it will transmit the password for the next level (bandit21).<br>NOTE: Try connecting to your own network daemon to see if it works as you think</p>
<br>

### 【翻訳】
<p style="font-size:120%;">ホームディレクトリには、次のことを行うsetuidバイナリがあります。コマンドライン引数として指定したポートでlocalhostに接続します。次に、接続から1行のテキストを読み取り、それを前のレベルのパスワード（bandit20）と比較します。パスワードが正しければ、次のレベル（bandit21）のパスワードを送信します。

注：自分のネットワークデーモンに接続して、思ったとおりに機能するかどうかを確認してください</p>
<br>

### 【回答】
<span>この問題を解くのはすこし手間がかかります。<br>ターミナルを2つ準備し、1つはポートを開けて通信を待ちbandit20のパスワードを送ります。<br>2つ目は開けたポートにsetuidファイルを使って接続をします。するとそのレスポンスとしてパスワードが返ってきます </span>

<br>

## Terminal1
```
※nc -lvp ポート番号（待ち受ける場合)
bandit20@bandit:~$ echo "GbKksEFF4yrVs6il55v6gwY5aVje5f0j" | nc -lvp ポート番号
```
## Terminal2
```
bandit20@bandit:~$ ./suconnect ポート番号
```

># Lv-21
### 【問題】
<p style="font-size:90%;">A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">プログラムは、時間ベースのジョブスケジューラであるcronから定期的に自動的に実行さ れます。/etc/cron.d/で構成を確認し、実行されているコマンドを確認します。</p>
<br>

### 【回答】
<span>この問題はジョブを管理しているcron(/etc/cron.d/)が構成している実行ファイルを確認しパスワードを取得する問題です。<br>まずは/etc/cron.dディレクトリを見てみましょう、cron_job_bandit22があるので中身をみて実行ファイルを探しましょう。<br>実行ファイルの中身を見てみるとcatで指定のディレクトリへパスワードを上書きしていますね。あとは上書きされたファイルをcatで見てあげれば終わりですね</span>

<br>

```
bandit21@bandit:~$ cd /etc/cron.d;ls
bandit21@bandit:~$ cat cronjob_bandit22
bandit21@bandit:~$ cat /usr/bin/cronjob_bandit22.sh
bandit21@bandit:~$ cat /tmp/文字列
```

># Lv-22
### 【問題】
<p style="font-size:90%;">A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.<br>
NOTE: Looking at shell scripts written by other people is a very useful skill. The script for this level is intentionally made easy to read. If you are having problems understanding what it does, try executing it to see the debug information it prints.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">プログラムは、時間ベースのジョブスケジューラであるcronから定期的に自動的に実行されます。/etc/cron.d/で構成を確認し、実行されているコマンドを確認します。

注：他の人が書いたシェルスクリプトを見るのは非常に便利なスキルです。このレベルのスクリプトは、意図的に読みやすくしています。それが何をするのか理解するのに問題がある場合は、それを実行して、それが出力するデバッグ情報を確認してみてください。</p>
<br>

### 【回答】
<span>今回の問題も以前の問題同様にスクリプトファイルを見てみましょう<br>すると各変数やコマンドを整理すると各ユーザのパスワード保存ディレクトリの内容を/tmp/「そのユーザ名をmd5化した文字列」先へ上書きとなっています<br>後半の知りたいユーザのパスワードユーザをmd5化した値が分かればいいのでコードの2行目をターミナルで実行しましょう※変更箇所あり<br>あとはcatで/tmpの中を見ればパスワードがわかりますね</span>

<br>

```
bandit22@bandit:/$cat /etc/cron.d/cronjob_bandit23
bandit22@bandit:/$cat /usr/bin/cronjob_bandit23.sh
bandit22@bandit:/$echo I am user bandit23 | md5sum | cut -d ' ' -f 1
bandit22@bandit:/$cat /tmp/文字列
```


># Lv-23
### 【問題】
<p style="font-size:90%;">A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.
<br>NOTE: This level requires you to create your own first shell-script. This is a very big step and you should be proud of yourself when you beat this level!
<br>NOTE 2: Keep in mind that your shell script is removed once executed, so you may want to keep a copy around…</p>
<br>

### 【翻訳】
<p style="font-size:120%;">プログラムは、時間ベースのジョブスケジューラであるcronから定期的に自動的に実行さ れます。/
etc/cron.d/で構成を確認し、実行されているコマンドを確認します。

注：このレベルでは、独自の最初のシェルスクリプトを作成する必要があります。これは非常に大きなステップであり、このレベルをクリアしたときは自分を誇りに思う必要があります。

注2：シェルスクリプトは実行されると削除されるため、コピーを残しておくことをお勧めします…</p>
<br>

### 【回答】
<span>今回の問題は少しややこしいです。<br>前回同様実行されるスクリプトファイルを見るのですがこのスクリプトの内容を理解していないと解けません<br>これは/var/spool/$mynameのパスにある全てのファイルが60秒後に削除され所有者がbandit23なら１回実行するというスクリプトです<br>なので/var/spool/bandit24にパスワードを取得するようなスクリプトファイルをおいてあげれば良いわけです。</span>

<br>

```
bandit23@bandit:~$ mkdir /tmp/bandit23
bandit23@bandit:~$ cd /tmp/bandit23
bandit23@bandit:/tmp/bandit23$ touch hoge.txt
bandit23@bandit:/tmp/bandit23$ chmod 777 hoge.txt
bandit23@bandit:/tmp/bandit23$ vi script.sh
bandit23@bandit:/tmp/bandit23$ chmod 777 script.sh
bandit23@bandit:/tmp/bandit23$ cp script.sh /var/spool/bandit24

---------- 1 minutes later.... ----------

bandit23@bandit:/tmp/bandit23$ cat hoge.txt
```

## 【script.sh】
```
#!/bin/bash
cat /etc/bandit_pass/bandit24 > /tmp/bandit23/hoge.txt
```

># Lv-24

### 【問題】
<p style="font-size:90%;">A daemon is listening on port 30002 and will give you the password for bandit25 if given the password for bandit24 and a secret numeric 4-digit pincode. There is no way to retrieve the pincode except by going through all of the 10000 combinations, called brute-forcing.</p>

### 【翻訳】
<p style="font-size:120%;">デーモンはポート30002でリッスンしており、bandit24のパスワードと秘密の数字の4桁のPINコードが与えられた場合、bandit25のパスワードを提供します。ブルートフォーシングと呼ばれる10000の組み合わせをすべて実行する以外に、ピンコードを取得する方法はありません。</p>

### 【回答】

指定のポート宛てに通信すると、**bandit24**のパスワードともう一つ４桁のパスコードが対話形式で聞かれます
４桁の総当たりを毎回打ち込むのは大変なのでシェルスクリプトファイルを作成し、通信する際にパイプを使い総当たり攻撃（ブルートフォース攻撃）をするコードを実行させパスワードを取得する


```
bandit24@bandit:mkdir /tmp/bandit24
bandit24@bandit:cd /tmp/bandit24
bandit24@bandit:vi script.sh
bandit24@bandit:chmod 777 script.sh
bandit24@bandit:./script.sh

> I am the pincode checker for user bandit25. Please enter the password for user bandit24 and the secret pincode on a single line, separated by a space.
Wrong! Please enter the correct pincode. Try again.
> Wrong! Please enter the correct pincode. Try again.
> ....n回
> Correct! The password of user bandit25 is "パスワード" 
```


### 【Script】
```
#!/binbash                                                
# ポート通信コマンド
nc_command="nc localhost 30002"
# わかっているパスワード変数
pass="UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ"
# ブルートフォース攻撃用関数
BlueForce(){
  for i in {1000..9999};
  do
    echo "$pass $i"
  done
}
# 実行
BlueForce | $nc_command;
```
># Lv-25
### 【問題】
<p style="font-size:90%;">Logging in to bandit26 from bandit25 should be fairly easy… The shell for user bandit26 is not /bin/bash, but something else. Find out what it is, how it works and how to break out of it.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">bandit25からbandit26へのログインはかなり簡単なはずです…ユーザーbandit26のシェルは/bin/ bashではなく、他のものです。それが何であるか、それがどのように機能するか、そしてそれから抜け出す方法を見つけてください。</p>
<br>

### 【回答】
<span>この問題はログインした後すぐに接続が切られてしまいます、ログインシェルに問題があるのでbandit25でログインシェルを確認しましょう。<br>確認するとusr/bin/showtextになっており通常のシェルと違う物を扱っていますね。実際にそのファイルを見てみましょう。<br>見てみるとmoreでtext.txtを開いていますね。その後にexit0で終了しているコードです。<br>moreはターミナルのサイズを変更するとコマンド待ち状態になりvを押すと開いているtext.txtをviで開いている状態になります。<br>コマンドが打てるならbandit26のパスワードを取得するコマンドを:eの後に打ち取得しましょう</span>

<br>

```
User@PC:$ ssh bandit26@bandit.labs.overthewire.org -p 2220 -i ~/.ssh/id_rsa_bandit26
> Connection to bandit.labs.overthewire.org closed.

exit

User@PC:$ ssh bandit25@bandit.labs.overthewire.org -p 2220
bandit25@bandit:cat /etc/passwd
bandit26:x:11026:11026:bandit level 26:/home/bandit26:/usr/bin/showtext

bandit25@bandit:cat /usr/bin/showtext
---
#!/bin/sh
export TERM=linux
more ~/text.txt
exit 0
---

User@PC:$ ssh bandit26@bandit.labs.overthewire.org -p 2220 -i ~/.ssh/id_rsa_bandit26
------------------------
--More--(16%)
:e /etc/bandit_pass/bandit26
```

># Lv-26

### 【問題】

<p style="font-size:90%;">Good job getting a shell! Now hurry and grab the password for bandit27!</p>
<br>

### 【翻訳】
<p style="font-size:120%;">シェルを取得するのは良い仕事です！急いで、bandit27のパスワードを入手してください。</p>
<br>

### 【回答】
<span>今回の問題は前回に引き続きターミナルのサイズを縮小して接続しviエディタを開きます。<br>setコマンドでshellという変数に/bin/bashを代入し:shellでbandit26のシェルを動かします。<br>あとはホームディレクトリにあるsuidファイルをbandit27のパスワードがある場所に使えばパスワードを取得できます</span>

<br>

```
User@PC:$ ssh bandit26@bandit.labs.overthewire.org -p 2220

-----
:set shell=/bin/bash
:shell
-----

---終了---

bandit26@bandit:~$ls
bandit26@bandit:~$ ./bandit27-do cat /etc/bandit_pass/bandit27
```

># Lv-27
### 【問題】
<p style="font-size:90%;">There is a git repository at ssh://bandit27-git@localhost/home/bandit27-git/repo. The password for the user bandit27-git is the same as for the user bandit27.<br>
Clone the repository and find the password for the next level.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">ssh://bandit27-git@localhost/home/bandit27-git/repoにgitリポジトリがあります。ユーザーのパスワードは、ユーザーのパスワードbandit27-gitと同じbandit27です。リポジトリのクローンを作成し、次のレベルのパスワードを見つけます。</p>
<br>

### 【回答】
<span>今回の問題はGitの知識がひつようになりますので、Gitが何かわからない人は以下のサイトを参考にしてください<br>[サル先生のGit入門](https://backlog.com/ja/git-tutorial/)<br>

問題文通りこのsshリポジトリURLをcloneする必要があるのでtmp内にディレクトリを作成しそこにcloneしましょう<br>
cloneできたらディレクトリ内を確認しREADMEファイルを見てみましょう。そこにパスワードがあるはずです
</span>



<br>

```
bandit27@bandit:/tmp$ mkdir bandit27
bandit27@bandit:/tmp$ cd bandit27
bandit27@bandit:/tmp/bandit27$ git clone ssh://bandit27-git@localhost/home/bandit27-git/repo

bandit27@bandit:/tmp/bandit27$ cd repo
bandit27@bandit:/tmp/bandit27$ ls
bandit27@bandit:/tmp/bandit27$ cat README
```

># Lv-28
### 【問題】
<p style="font-size:90%;">There is a git repository at ssh://bandit28-git@localhost/home/bandit28-git/repo. The password for the user bandit28-git is the same as for the user bandit28.

Clone the repository and find the password for the next level.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">ssh://bandit28-git@localhost/home/bandit28-git/repoにgitリポジトリがあります。ユーザーのパスワードは、ユーザーのパスワードbandit28-gitと同じbandit28です。

リポジトリのクローンを作成し、次のレベルのパスワードを見つけます。</p>
<br>

### 【回答】
<span>今回の問題は27とやることは変わらないので割愛します</span>

<br>

># Lv-29

### 【問題】
<p style="font-size:90%;">There is a git repository at ssh://bandit29-git@localhost/home/bandit29-git/repo. The password for the user bandit29-git is the same as for the user bandit29.<br>
Clone the repository and find the password for the next level.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">ssh://bandit29-git@localhost/home/bandit29-git/repoにgitリポジトリがあります。ユーザーのパスワードは、ユーザーのパスワードbandit29-gitと同じbandit29です。

リポジトリのクローンを作成し、次のレベルのパスワードを見つけます。</p>
<br>

### 【回答】
<span>今回の問題はいままで通りREADMEファイルに書いてある単純な問題ではありません。<br>同じようにREADMEファイルを見てみる、パスワードの部分がありません。<br>git logで変更された可能性もあるのでは？という推測の元見てみても何もありません。<br>そこでファイルのパスワードの場所にあった英文を訳すと「本番環境にはない」とあるので本番以外の環境ならあるのでは？という推測でリモートのブランチを確認してみましょう。<br>するとdevというブランチが見つかるのでこれに切り替えlog+変更内容表示で見てみましょう。<br>masterのログにはなかったパスワード変更内容がわかりましたね</span>

<br>

```
bandit29@bandit:/tmp$ mkdir bandit29
bandit29@bandit:/tmp$ cd bandit29
bandit29@bandit:/tmp/bandit29$ git clone ssh://bandit29-git@localhost/home/bandit29-git/repo

bandit29@bandit:/tmp/bandit29$ cd repo
bandit29@bandit:/tmp/bandit29$ ls
bandit29@bandit:/tmp/bandit29$ cat README.md

-----
- username: bandit30
- password: <no passwords in production!>
-----

bandit29@bandit:/tmp/bandit29$ git branch -r
  origin/HEAD -> origin/master
  origin/dev
  origin/master
  origin/sploits-dev

bandit29@bandit:/tmp/bandit29$ git checkout dev
bandit29@bandit:/tmp/bandit29$ git log -p

```


<br>

># Lv-30
### 【問題】
<p style="font-size:90%;">There is a git repository at ssh://bandit30-git@localhost/home/bandit30-git/repo. The password for the user bandit30-git is the same as for the user bandit30.<br>
Clone the repository and find the password for the next level.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">ssh://bandit30-git@localhost/home/bandit30-git/repoにgitリポジトリがあります。ユーザーのパスワードは、ユーザーのパスワードbandit30-gitと同じbandit30です。<br>
リポジトリのクローンを作成し、次のレベルのパスワードを見つけます。</p>
<br>

### 【回答】
<span>今回も同じようにファイルやログ、ブランチなど確認してパスワードを探してみましょう。<br>しかしどうでしょう、何も手掛かりがありません。<br>ですがgitにはlogやbranch以外にtagというものがあります。もしかしたらこれを設定しているかもしれないのでgit tagで確認してみましょう。<br>やはりありましたね。これをcatで開くとパスワード取得できますね</span>

<br>

```
bandit30@bandit:/tmp$ mkdir bandit30
bandit30@bandit:/tmp$ cd bandit30
bandit30@bandit:/tmp/bandit30$ git clone ssh://bandit30-git@localhost/home/bandit30-git/repo

bandit30@bandit:/tmp/bandit30$ cd repo
bandit30@bandit:/tmp/bandit30$ ls
bandit30@bandit:/tmp/bandit30$ cat README.md

-----
just an epmty file... muahaha
-----

bandit30@bandit:/tmp/bandit30$ git branch -r
  origin/HEAD -> origin/master
  origin/master

bandit30@bandit:/tmp/bandit30/repo$ git log -p
  commit 3aefa229469b7ba1cc08203e5d8fa299354c496b
  Author: Ben Dover <noone@overthewire.org>
  Date:   Thu May 7 20:14:54 2020 +0200

     initial commit of README.md

bandit30@bandit:/tmp/bandit30/repo$ git tag
> secret
bandit30@bandit:/tmp/bandit30/repo$ git show secret

```

># Lv-31

### 【問題】
<p style="font-size:90%;">There is a git repository at ssh://bandit31-git@localhost/home/bandit31-git/repo. The password for the user bandit31-git is the same as for the user bandit31.<br>
Clone the repository and find the password for the next level.</p>
<br>

### 【翻訳】
<p style="font-size:120%;">にgitリポジトリがありますssh://bandit31-git@localhost/home/bandit31-git/repo。ユーザーのパスワードは、ユーザーのパスワードbandit31-gitと同じbandit31です。</p>
<br>

### 【回答】
<span>今回の問題は少し特殊です。<br>以前同様ファイルやログを確認するとREADMEファイルに「リモートリポジトリのmasterブランチにkey.txtファイルをpushしてください」と書かれています。<br>なので指定のファイルをpushしましょう。すると簡単にパスワードが取得できました</span>

<br>

```
bandit31@bandit:~$ mkdir /tmp/bandit31 
bandit31@bandit:~$ cd /tmp/bandit31
bandit31@bandit:/tmp/bandit31$ git clone ssh://bandit31-git@localhost/home/bandit31-git/repo
bandit31@bandit:/tmp/bandit31$ cd repo/
bandit31@bandit:/tmp/bandit31/repo$ ls
key.txt  README.md
bandit31@bandit:/tmp/bandit31/repo$ cat README.md 
---
This time your task is to push a file to the remote repository.
Details:
    File name: key.txt
    Content: 'May I come in?'
    Branch: master
---
bandit31@bandit:/tmp/bandit31/repo$ git add key.txt 
bandit31@bandit:/tmp/bandit31/repo$ git commit -m "test"
---
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
---
bandit31@bandit:/tmp/bandit31/repo$ git push origin master
---
Could not create directory '/home/bandit31/.ssh'.
The authenticity of host 'localhost (127.0.0.1)' can't be established.....

bandit31-git@localhost's password:
Counting objects: 3, done.
Delta compression using up to 2 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 320 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote: ### Attempting to validate files... ####
remote:
remote: .oOo.oOo.oOo.oOo.oOo.oOo.oOo.oOo.oOo.oOo.
remote:
remote: Well done! Here is the password for the next level:
remote: 56a9bf19c63d650ce78e6ec0354ee45e
remote:
remote: .oOo.oOo.oOo.oOo.oOo.oOo.oOo.oOo.oOo.oOo.
remote:
---
```

># Lv-32
### 【問題】
<p style="font-size:90%;">After all this git stuff its time for another escape. Good luck!</p>
<br>

### 【翻訳】
<p style="font-size:120%;">結局のところ、これgitは別の脱出のための時間です。幸運を！</p>
<br>

### 【回答】
<span>この問題はそもそも問題文が読み取りづらいです。<br>まず33のパスワードが取得できたのでログインするとインタラクティブシェルに入りますがここではコマンドなどが使えませんのでbashシェルを使うために$0変数を打ちbashシェルに入ります。<br>※$0には/bin/bashが入っています<br>入った後はwhoamiコマンドで誰なのか確認しbandit33になっているのでいつもの場所のパスワードを開いてパスワードを取得しましょう
</span>

<br>

```
User@PC:$ ssh bandit32@bandit.labs.overthewire.org -p 2220
WELCOME TO THE UPPERCASE SHELL
>>$0

$ whoami
bandit33

$ pwd
/home/bandit32

$ cat /etc/bandit_pass/bandit33 
c9c3199ddf4121b10cf581a98d51caee
```
